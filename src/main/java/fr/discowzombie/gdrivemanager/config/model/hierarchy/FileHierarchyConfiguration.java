package fr.discowzombie.gdrivemanager.config.model.hierarchy;

import java.util.Set;

public class FileHierarchyConfiguration {
    public String id;
    public Set<AccessLevelConfiguration> groups;
    public Set<AccessLevelConfiguration> users;
}
