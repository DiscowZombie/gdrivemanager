package fr.discowzombie.gdrivemanager.config.model.hierarchy;

import fr.discowzombie.gdrivemanager.api.PermissionUpdatePolicy;
import fr.discowzombie.gdrivemanager.api.Role;

public class AccessLevelConfiguration {
    public String id;
    public Role permission;
    public PermissionUpdatePolicy policy = PermissionUpdatePolicy.DEFAULT_VALUE;
}
