package fr.discowzombie.gdrivemanager.config.model.groups;

import java.util.Set;

public class GroupConfiguration {
    public String name;
    public Set<String> members;
}
