/*
 * Copyright (c) STELERIO SAS - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by STELERIO SAS <contact@stelerio.com>, June 2019
 */

package fr.discowzombie.gdrivemanager.config;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.io.File;
import java.io.IOException;

/**
 * @author DiscowZombie (Mathéo CIMBARO)
 * @version 1.0
 */
public final class ConfigurationReader {

    private static final ObjectMapper MAPPER = createMapper();

    private ConfigurationReader() {
    }

    public static <T> T readConfiguration(File file, Class<T> configuration) throws IOException {
        return MAPPER.readValue(file, configuration);
    }

    private static ObjectMapper createMapper() {
        final ObjectMapper mapper = new ObjectMapper();
        // General
        mapper.enable(Feature.ALLOW_COMMENTS); // Allow C/C++ style comments in JSON
        // Serialization
        mapper.enable(SerializationFeature.INDENT_OUTPUT); // Enable pretty-printing
        // Deserialization
        mapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT); // Empty string "" will become null object (coercion)
        // Custom Serialization/Deserialization
        final SimpleModule module = new SimpleModule();
        mapper.registerModule(module);
        // Return-type
        return mapper;
    }
}
