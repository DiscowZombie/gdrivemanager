package fr.discowzombie.gdrivemanager.api;

public enum Role {

    READER("reader"),
    COMMENTER("commenter"),
    WRITER("writer"),
    FILE_ORGANIZER("fileOrganizer"),
    ORGANIZER("organizer");

    public static final Role[] VALUES = values();
    public final String apiName;

    Role(String apiName) {
        this.apiName = apiName;
    }

    public static Role fromName(String apiName) {
        for (Role value : VALUES) {
            if (value.apiName.equalsIgnoreCase(apiName))
                return value;
        }
        return null;
    }
}
