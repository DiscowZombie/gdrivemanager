package fr.discowzombie.gdrivemanager.api;

public enum PermissionUpdatePolicy {

    /**
     * Create the permission if she does not previously exist. Else, do nothing.
     */
    CREATE_IF_ABSENT,

    /**
     * Create or update the permission. This can have unexpected effect such as decreasing user permissions.
     */
    CREATE_OR_UPDATE;

    public static final PermissionUpdatePolicy DEFAULT_VALUE = CREATE_OR_UPDATE;
}
