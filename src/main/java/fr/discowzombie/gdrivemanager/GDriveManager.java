package fr.discowzombie.gdrivemanager;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.User;
import fr.discowzombie.gdrivemanager.api.PermissionUpdatePolicy;
import fr.discowzombie.gdrivemanager.config.ConfigurationReader;
import fr.discowzombie.gdrivemanager.config.model.groups.BaseConfiguration;
import fr.discowzombie.gdrivemanager.config.model.groups.GroupConfiguration;
import fr.discowzombie.gdrivemanager.config.model.hierarchy.AccessLevelConfiguration;
import fr.discowzombie.gdrivemanager.config.model.hierarchy.FileHierarchyConfiguration;
import fr.discowzombie.gdrivemanager.config.model.hierarchy.HierarchyConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GDriveManager {

    // TODO: Supprimer des utilisateurs?

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH = "tokens";

    /**
     * Global instance of the scopes required by this quickstart.
     * If modifying these scopes, delete your previously saved tokens/ folder.
     */
    private static final List<String> SCOPES = Arrays.asList(DriveScopes.DRIVE, DriveScopes.DRIVE_FILE);

    public static void main(String[] args) throws IOException, GeneralSecurityException {
        final BaseConfiguration configuration = ConfigurationReader
                .readConfiguration(new File("./src/main/resources/groups.json"), BaseConfiguration.class);
        final HierarchyConfiguration hierarchyConfiguration = ConfigurationReader
                .readConfiguration(new File("./src/main/resources/hierarchy.json"), HierarchyConfiguration.class);

        final Map<String, GroupConfiguration> groups = configuration.groups.stream()
                .collect(Collectors.toMap(g -> g.name, g -> g));

        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        final Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                .build();

        // Pour chaque fichier/dossier, dont on doit définir les permissions
        com.google.api.services.drive.model.File retrievedFile;
        List<String> ownersEmails;
        GroupConfiguration groupConfiguration;
        for (final FileHierarchyConfiguration permission : hierarchyConfiguration.permissions) {
            retrievedFile = service.files()
                    .get(permission.id)
                    .setFields("id,owners,permissions")
                    .execute();

            // Fichier introuvable
            if (retrievedFile == null) {
                System.out.println("File with id=" + permission.id + " was not found");
                continue;
            }

            ownersEmails = getOwnersEmails(retrievedFile.getOwners());

            // Si les utilisateurs sont spécifiées
            if (permission.users != null) {
                // Pour chaque utilisateur
                for (final AccessLevelConfiguration user : permission.users) {
                    if (ownersEmails.contains(user.id)) // Ignorer l'owner
                        continue;
                    createOrUpdatePermission(service.permissions(), retrievedFile, user, user.id);
                }
            }

            // Si les groupes sont spécifiés
            if (permission.groups != null) {
                // Pour chaque group
                for (final AccessLevelConfiguration group : permission.groups) {
                    // group.id -> nom du groupe
                    groupConfiguration = groups.get(group.id);

                    if (groupConfiguration == null) {
                        System.out.println("Group with id=" + group.id + " was not found");
                        continue;
                    }

                    for (final String member : groupConfiguration.members) {
                        if (ownersEmails.contains(member)) // Ignorer l'owner
                            continue;
                        createOrUpdatePermission(service.permissions(), retrievedFile, group, member);
                    }
                }
            }
        }

        HTTP_TRANSPORT.shutdown();
    }

    @Nullable
    private static Permission createOrUpdatePermission(@NotNull Drive.Permissions permissions,
                                                       @NotNull com.google.api.services.drive.model.File file,
                                                       @NotNull AccessLevelConfiguration levelConfiguration,
                                                       @NotNull String emailAddress) throws IOException {
        Permission perm, prevPermission = getPermission(file.getPermissions(), emailAddress);

        // Créer les permissions dans tous les cas
        if (prevPermission == null) {
            perm = new Permission()
                    .setRole(levelConfiguration.permission.apiName)
                    .setType("user")
                    .setEmailAddress(emailAddress);

            return permissions.create(file.getId(), perm).setSendNotificationEmail(false).execute();
        }
        // Mettre à jour les permissions
        else if (levelConfiguration.policy == PermissionUpdatePolicy.CREATE_OR_UPDATE) {
            perm = new Permission()
                    .setId(prevPermission.getId())
                    .setRole(levelConfiguration.permission.apiName);

            return permissions.update(file.getId(), prevPermission.getId(), perm).execute();
        }

        // There are nothing to do !
        return null;
    }

    /**
     * Creates an authorized Credential object.
     *
     * @param HTTP_TRANSPORT The network HTTP Transport.
     * @return An authorized Credential object.
     * @throws IOException If the credentials.json file cannot be found.
     */
    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
        try (final FileReader reader = new FileReader(new File("./src/main/resources/credentials.json"))) {
            GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, reader);

            // Build flow and trigger user authorization request.
            final GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                    HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                    .setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
                    .setAccessType("offline")
                    .build();
            final LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
            return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
        }
    }

    private static Permission getPermission(List<Permission> permissions, String userEmail) {
        for (Permission permission : permissions) {
            if (permission.getEmailAddress() != null && permission.getEmailAddress().equalsIgnoreCase(userEmail))
                return permission;
        }
        return null;
    }

    private static List<String> getOwnersEmails(List<User> owners) {
        final List<String> list = new ArrayList<>(owners.size());
        for (User owner : owners)
            list.add(owner.getEmailAddress());
        return list;
    }
}
